// Assignment 3 - Pointers
// James Darling

#include <iostream>
#include <conio.h>



// TODO: Implement the "SwapIntegers" function


// Do not modify the main function!
void SwapIntegers(int *firstp, int *secondp)
{

	int x = *firstp;
	*firstp = *secondp;
	*secondp = x;
	


}
int main()
{
	int first = 0;
	int second = 0;
	

	std::cout << "Enter the first integer: ";
	std::cin >> first;

	std::cout << "Enter the second integer: ";
	std::cin >> second;

	std::cout << "\nYou entered:\n";
	std::cout << "first: " << first << "\n";
	std::cout << "second: " << second << "\n";

	SwapIntegers(&first,&second);
	
	std::cout << "\nAfter swapping:\n";
	std::cout << "first: " << first << "\n";
	std::cout << "second: " << second << "\n";

	std::cout << "\nPress any key to quit.";

	_getch();
	return 0;
}

